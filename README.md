#django-ydd-deploy-fabric

The fabric that helping us deploy our projects.

![First time deploy a project to a node][1]

##Install

        pip install -U -e git+git@bitbucket.org:voleg/django-ydd-deploy-fabric.git#egg=django-ydd-deploy-fabric

Add your local **fab file** in the project root and add these lines:

        from fabric.contrib import django
        django.settings_module('myproject.settings')
        from deploy_fabric.fabfile import *

where 'myproject.settings', you need to replace with yours.

##Usage

After installing type in console **fab help** to get the list of available commands.


##Examples

First time deploy:

        $ fab prod deploy:init
        or
        $ fab prod verbose deploy:init


[1]: https://bitbucket.org/ydd/django-ydd-deploy-fabric/raw/fe974175d8b1bd14e9e0013293f7342a037621b4/docs/fab_prod_verbose_deploy_init_0.png
