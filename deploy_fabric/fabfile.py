# coding: utf-8
import os
import re
import sys
from os.path import isfile
from datetime import datetime
import urllib2
from contextlib import contextmanager as _contextmanager
try:
    from fabric.api import env, run, local, execute, require, put, task, get
    from fabric.context_managers import lcd, prefix, cd, hide, show
    from fabric.decorators import with_settings
    from fabric.state import output
    from fabric.colors import *
    from fabric.contrib.files import upload_template, exists
    from fabric.contrib import django
except ImportError, e:
    print('Install Fabric!')

from django.conf import settings
from .fab_helpers import *

output['warnings'] = False
output['status'] = False
output['stderr'] = False
output['stdout'] = False
output['running'] = False
output['debug'] = False

CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
REQUIREMENTS = getattr(settings, 'PROJECT_REQUIREMENTS', 'requirements')
PROJECT = getattr(settings, 'PROJECT_NAME', None)
PROJECT_DIR = getattr(settings, 'PROJECT_DIR', PROJECT)
REPO = getattr(settings, 'REPO', None)
VENV_DIR = getattr(settings, 'VENV_DIR_NAME', 'env')

env.use_ssh_config = True
env.shell = '/bin/bash -l -i -c'
MSG_IDENT = cyan('>>> ')
HAPPY = green('<<< OK >>>')
FEEL_SAD = red('<<< Failed >>>')
SKIP = yellow('<<< Skip >>>')
ERR_IDENT = red('     >>> ')
env.project = PROJECT
env.project_dir = PROJECT_DIR
env.root_dir = getattr(settings, 'DEPLOY_DIRECTORY', None)
env.supervisor_dir = '/'.join([CURRENT_DIR, 'supervisor/'])
env.supervisor_template = ''.join([env.supervisor_dir, 'template.conf'])
env.supervisor_service_conf_dir = getattr(settings, 'SUPERVISOR_CONF_DIR', '/etc/supervisor/conf.d')
gunicorn_cmd = '%(worker)s --log-level=debug --log-file=%(worker_log)s ' \
               '--preload --workers %(workers_number)s --bind %(worker_addr)s:%(worker_port)s ' \
               '%(project)s.wsgi:application'
gunicorn_log_dir = getattr(settings, 'GUNICORN_LOG_DIR', '/var/log/gunicorn/')
gunicorn_log = gunicorn_log_dir + '%s_gunicorn.log'
env.newrelic_cfg = '%s/newrelic.ini' % env.root_dir
env.newrelic_template = '/'.join([CURRENT_DIR, 'newrelic', 'template.ini'])
env.newrelic_deploy_url = getattr(settings, 'NEWRELIC_DEPLOY_URL', 'https://rpm.newrelic.com/deployments.xml')
env.newrelic_license_key = getattr(settings, 'NEWRELIC_LICENSE_KEY', None)
env.app_name = getattr(settings, 'NEWRELIC_APP_NAME', PROJECT)
env.newrelic_log_dir = getattr(settings, 'NEWRELIC_LOG_DIR', '/var/log/newrelic')
env.unix_user = getattr(settings, 'WEBSERVICE_UNIX_USER', 'www-data')
env.unix_group = getattr(settings, 'WEBSERVICE_UNIX_GROUP', 'www-data')
env.nginx_conf_dir = getattr(settings, 'NGINX_CONF_DIR', '/etc/nginx/conf.d')
env.nginx_template = '/'.join([CURRENT_DIR, 'nginx', 'template.conf'])
ALL_PACKAGES = FileToList(REQUIREMENTS)


def _monitor_with_newrelic(node):
    if getattr(settings, '%s_MONITORING' % node, False) and hasattr(env, 'newrelic_cfg')\
    and getattr(settings, 'NEWRELIC_RUN_PROGRAM', False):
        return True
    return False

def show_status(*args):
    if all(map(lambda a: a.succeeded, args)):
        print(HAPPY)
    else:
        print(FEEL_SAD)
        for a in args:
            if a and getattr(env, 'verbose', False):
                print('%s $ %s' % (ERR_IDENT, a.command))
                print('%s > %s' % (ERR_IDENT, red(a)))


@task
def prod():
    """production node $ fab prod <command>"""
    env.hosts = getattr(settings, 'PRODUCTION_NODES', None)
    env.branch = getattr(settings, 'PRODUCTION_BRANCH', 'master')
    env.domain = getattr(settings, 'PRODUCTION_FQDN', None)
    env.db = getattr(settings, 'PRODUCTION_DB', None)
    env.venv_dir = VENV_DIR
    env.activate = 'source %s/bin/activate' % env.venv_dir
    env.directory = '/'.join([env.root_dir, env.branch])
    env.venv_directory = '%(directory)s/%(venv_dir)s/bin' % env
    env.server_type = getattr(settings, 'WEBSERVER', 'nginx')
    env.settings_dir = ''.join([env.project_dir, PROJECT])
    env.nginx_conf = '%s-%s.conf' % (PROJECT, env.branch)
    env.supervisor_conf = '%s-%s.conf' % (PROJECT, env.branch)
    env.worker_name = '%s_%s' % (PROJECT, env.branch)
    env.run_program = env.venv_directory + '/' + getattr(settings, 'NEWRELIC_RUN_PROGRAM', '') + ' '\
        if _monitor_with_newrelic('PRODUCTION') else ''
    env.worker = '%(run_program)s%(venv_directory)s/gunicorn' % env
    env.worker_port = getattr(settings, 'PRODUCTION_WORKER_PORT', '8002')
    env.worker_addr = getattr(settings, 'WORKER_ADDR', '127.0.0.1')
    env.workers_number = getattr(settings, 'PRODUCTION_WORKERS_NUMBER', '4')
    env.worker_log = gunicorn_log % env.worker_name
    env.newrelic_log = '/'.join([env.newrelic_log_dir, '%s_%s.log' % (env.project, env.branch)])
    env.sv_env_path = 'PATH=%(venv_directory)s' % env
    env.sv_env_path += ',NEW_RELIC_CONFIG_FILE=%(newrelic_cfg)s' % env\
        if _monitor_with_newrelic('PRODUCTION') else ''
    env.sv_cmd = gunicorn_cmd % env
    env.sv_dir = '/'.join([env.directory, env.project_dir])
    env.additional_packages = [
        'gunicorn',
        'newrelic' if _monitor_with_newrelic('PRODUCTION') else ''
    ]


@task
def dev():
    """development node $ fab dev <command>"""
    env.hosts = getattr(settings, 'STAGING_NODES', None)
    env.branch = getattr(settings, 'STAGING_BRANCH', 'dev')
    env.domain = getattr(settings, 'STAGING_FQDN', None)
    env.db = getattr(settings, 'STAGING_DB', None)
    env.venv_dir = VENV_DIR
    env.activate = 'source %s/bin/activate' % env.venv_dir
    env.directory = '/'.join([env.root_dir, env.branch])
    env.venv_directory = '%(directory)s/%(venv_dir)s/bin' % env
    env.settings_dir = ''.join([env.project_dir, PROJECT])
    env.server_type = getattr(settings, 'WEBSERVER', 'nginx')
    env.nginx_conf = '%s-%s.conf' % (PROJECT, env.branch)
    env.supervisor_conf = '%s-%s.conf' % (PROJECT, env.branch)
    env.worker_name = '%s_%s' % (PROJECT, env.branch)
    # for newrelic luncher
    env.run_program = '/'.join([env.venv_directory, getattr(settings, 'NEWRELIC_RUN_PROGRAM', ''), ' '])\
        if _monitor_with_newrelic('STAGING') else ''
    env.worker = '%(run_program)s%(venv_directory)s/gunicorn' % env
    env.worker_port = getattr(settings, 'STAGING_WORKER_PORT', '4002')
    env.worker_addr = getattr(settings, 'WORKER_ADDR', '127.0.0.1')
    env.workers_number = getattr(settings, 'STAGING_WORKERS_NUMBER', '1')
    env.worker_log = gunicorn_log % env.worker_name
    env.sv_env_path = 'PATH=\'%s\'' % env.venv_directory
    env.sv_cmd = gunicorn_cmd % env
    env.sv_dir = '/'.join([env.directory, env.project_dir])
    env.additional_packages = [
        'gunicorn',
        'newrelic' if _monitor_with_newrelic('STAGING') else ''
    ]


@task
def verbose():
    """ Try to be little more verbose $ fab <node> verbose deploy"""
    env.verbose = True


@with_settings(warn_only=True)
def init(*args):
    """ Creates project directory"""
    require('hosts')
    run('mkdir %s' % env.root_dir)


@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield


def check_log():
    def _check(f, e):
        if not exists(f):
            run('> %s' % f)
        run('chown %(unix_user)s:%(unix_group)s' % e + ' %s' % f)
    if not exists(gunicorn_log_dir):
        run('mkdir %s' % gunicorn_log_dir)
    _check(env.worker_log, env)
    run('chown %(unix_user)s:%(unix_group)s %(worker_log)s' % env)
    if 'newrelic' in env.worker:
        _check(env.newrelic_log, env)


@task
@with_settings(warn_only=True)
def clone():
    """ clone a git repository $ fab <node> clone"""
    print('%s Cloning project' % MSG_IDENT),
    execute(init)
    run('apt-get -qq install git')
    with cd(env.directory):
        if env.project_dir and env.project_dir != PROJECT:
            show_status(
                run('git clone -q %s %s' % (REPO, env.project_dir))
            )
        else:
            show_status(run('git clone %s' % REPO)),
        if env.branch:
            run('git checkout %s' % env.branch)
    execute(set_perms)


def clone_media():
    """Sync the /media/ directory"""
    execute(push_media)
    execute(pull_media)


def push_media():
    for host in env.hosts:
        print('%s Coping media files' % MSG_IDENT)
        local('rsync -ur --progress media/ %s:%s/%s/media/' % (host, env.directory, env.project_dir))
        print('%s Changing access rights %s/%s' % (MSG_IDENT, env.directory, env.project_dir))
        run('chmod -R 777 %s/%s' % (env.directory, env.project_dir))


def pull_media():
    for host in env.hosts:
        print('%s Downloading media files' % MSG_IDENT)
        local('rsync -ur --progress %s:%s/%s/media/ media/' % (host, env.directory, env.project_dir))


# deprecated and should be removed soon
def link_static():
    run('rm http/static')
    with cd('http'):
        run('ln -s ../%s/static static' % env.project_dir)


@task
def remove_project():
    """ remove project directory """
    with cd(env.directory):
        print('%s Removing the project' % MSG_IDENT)
        run('rm -rf %s' % env.project_dir)


@task
def remove_venv():
    """ remove project virtualenv """
    with cd(env.directory):
        print('%s Removing virtualenv dir' % MSG_IDENT)
        run('rm -rf %s' % env.venv_dir)

@task
def rm():
    print('%s Removing all' % MSG_IDENT)
    run('rm -rf %s' % env.directory)


@task
def installed():
    """ Show installed packages """
    with virtualenv():
        installed = run('pip freeze')
        print('\n\n %s Installed:\n %s' % (MSG_IDENT, installed))


@with_settings(warn_only=True)
def install_packages():
    ident = cyan('     +')
    def setup(package):
        if run('pip -q install %s' % package).succeeded:
            sys.stdout.write(green(' > installed\n') if getattr(env, 'verbose', False) else green('.'))
            sys.stdout.flush()
        else:
            sys.stdout.write(red('> failed\n') if getattr(env, 'verbose', False) else red('E'))
            sys.stdout.flush()
    for package in filter(lambda x: not re.match(r'^#', x), ALL_PACKAGES):
        if not 'Fabric' in package:
            if getattr(env, 'verbose', False):
                sys.stdout.write('%s %s' % (ident, package.strip())),
                sys.stdout.flush()
            setup(package)
    if not env.additional_packages == []:
        for package in env.additional_packages:
            if getattr(env, 'verbose', False):
                sys.stdout.write('%s %s' % (ident, package.strip())),
                sys.stdout.flush()
            setup(package)
    sys.stdout.write('\n')


@task
def install():
    """ Install required packages $ fab <node> install"""
    print('%s Installing dependencies' % MSG_IDENT)
    try:
        if not env.activate == '':
            with virtualenv():
                execute(install_packages)
        else:
            execute(install_packages)
    except AttributeError:
        execute(install_packages)


@task
def uninstall():
    """ uninstall project packages """
    def remove(package):
        try:
            run('pip uninstall %s' % package)
        except:
            pass
    for package in ALL_PACKAGES:
        remove(package)
    if not env.additional_packages == []:
        for package in env.additional_packages:
            remove(package)
    run('pip freeze')


@task
def dump_db():
    """ dump a backup DB image $ fab <node> dump_db"""
    DB = env.db
    now = datetime.now()
    remote_dump_file = 'db_dump.sql'
    local_dump_dir = 'dumps'
    local_dump_file = '%s/db_dump_%s.sql' % (local_dump_dir, now.strftime('%Y-%m-%d_%H-%M-%S'))
    local('mkdir -p %s' % local_dump_dir)
    print('%s Creating DB dump' % MSG_IDENT),

    if 'postgresql' in DB['ENGINE']:
        show_status(
            run('pg_dump -h %s -U %s %s > %s' % (DB['HOST'], DB['USER'], DB['NAME'], remote_dump_file)),
            get(remote_dump_file, local_dump_file)
        )
    elif 'mysql' in DB['ENGINE']:
        show_status(
            run('mysqldump --user=%s --password=%s %s > %s' % (DB['HOST'], DB['USER'], DB['NAME'], remote_dump_file)),
            get(remote_dump_file, local_dump_file)
        )
    else:
        print(SKIP + yellow(' [only postgresql and mysql supported for now]'))


#todo refactor or remove this crap
@with_settings(warn_only=True)
def db_to_local():
    DB = env.db
    forein_dump_file = 'db_dump.sql'
    local_dump_dir = 'dumps'
    local_dump_file = 'db_dump_to_local.sql'
    try:
        local('mkdir %s' % local_dump_dir)
    except:
        pass
    print('%s Creating DB dump' % MSG_IDENT),
    if run('pg_dump -h %s -U %s %s > %s' % ('localhost', DB['USER'], DB['NAME'], forein_dump_file)).succeeded:
        print(green("OK"))
    print('%s coping to %s' % (MSG_IDENT, local_dump_dir))
    if local('scp %s:%s %s/%s' % (env.hosts[0], forein_dump_file, local_dump_dir, local_dump_file)).succeeded:
        print(green("OK"))
    run('rm %s' % forein_dump_file)
    local('dropdb -h localhost %s' % env.project_dir)
    local('createdb -h localhost -E UTF8 -T template0 --lc-collate="ru_RU.UTF-8" --lc-ctype="ru_RU.UTF-8" %s' % env.project_dir)
    local('psql -h localhost %s < %s/%s' % (env.project_dir, local_dump_dir, local_dump_file))


@task
@with_settings(warn_only=True)
def make_venv():
    """ create virtualenv on the node $ fab <node> make_venv
        Make shore your node have python version >= 2.7
    """
    run('mkdir -p %s' % env.directory)
    with cd(env.directory):
        print('%s Creating new viruatenv' % MSG_IDENT),
        if not exists('%s/bin/python' % env.venv_dir):
            show_status(run('virtualenv --no-site-packages %s' % env.venv_dir))
        else:
            print(SKIP + green(' [looks like, already exists]'))


@task
def make_ssh_key(host=None):
    """ create ssh rsa key pairs for node $ fab <node> make_ssh_key:host_name"""
    ssh_config_file = 'config'
    name = host
    print('%s Making SSH key' % MSG_IDENT)
    if name and host:
        run('mkdir -p ~/.ssh')
        with cd('~/.ssh'):
            run('ssh-keygen -t rsa -q -P \'\' -f %s ' % name)
            sshpubkey = run('cat %s.pub' % name)
            print(green('===< Copy it >===\n%s\n===< And use it >===' % sshpubkey))
            run('touch %s' % ssh_config_file)
            run('echo -e "Host %s\\n IdentityFile ~/.ssh/%s\\n" >> %s' % (host, name, ssh_config_file))
    else:
        print('Skiping...\nPlease use this option in following format fab <node> make_ssh_key:host_name')


@task
def workers_reload():
    """ restart gunicorn"""
    print("%s Restart %s workers " % (MSG_IDENT, env.worker_name)),
    show_status(run('supervisorctl restart %s' % env.worker_name))


@task
def push_supervisor_conf():
    """ update supervisor configuration """
    print("%s Updating supervisor config" % (MSG_IDENT)),
    if isfile(env.supervisor_template):
        template = env.supervisor_template
        conf = '/'.join([env.supervisor_service_conf_dir, env.supervisor_conf])
        show_status(
            upload_template(template, conf, context=env, backup=False, use_sudo=False),
            run('supervisorctl update')
        )
    else:
        print(red('%s supervisor template file not found!' % ERR_IDENT))


@task
def update_nginx_conf():
    """ update nginx config """
    require('domain')
    env.additional_domains = ''
    if isinstance(env.domain, (tuple, list)):
        env.additional_domains = ' '.join(env.domain[1:])
        env.domain = env.domain[0]

    if isfile(env.nginx_template):
        template = env.nginx_template
        conf = '/'.join([env.nginx_conf_dir, env.nginx_conf])
        upload_template(template, conf, context=env, backup=False, use_sudo=False)
    else:
        print(red('%s nginx template file not found!' % MSG_IDENT))


@task
def nginx_reload():
    """ reload nginx $ fab <node> server_restart """
    if 'nginx' in env.server_type:
        run('/etc/init.d/nginx reload')


@task
def nginx_stop():
    """ stop nginx $ fab <node> stop"""
    if 'nginx' in env.server_type:
        status = run('/etc/init.d/nginx stop')
        if 'nginx' in status:
            print('%s Service was stoped' % MSG_IDENT)


@task
def nginx_start():
    """ start nginx $ fab <node> start"""
    if 'nginx' in env.server_type:
        status = run('/etc/init.d/nginx start')
        if 'nginx' in status:
            print('%s Service was stoped' % MSG_IDENT)


def get_pull():
    print('%s Pulling changes on branch: ' % MSG_IDENT + cyan(env.branch)),
    show_status(
        run('git fetch'),
        run('git checkout %s' % env.branch),
        run('git pull --force origin %s' % env.branch),
        run('git checkout %s' % env.branch)
    )

def db_migration():
    with virtualenv():
        run('python %s/manage.py migrate' % env.project_dir)


def _send_deploy_info():
    data = {
        'deployment[app_name]': PROJECT,
        # 'deployment[application_id]': '',
        # 'deployment[description]': '',
        # 'deployment[revision]': '',
        # 'deployment[changelog]': '',
        'deployment[user]': os.environ['USER']
    }
    try:
        key = settings.NEW_RELIC_API_KEY
    except KeyError:
        print(u'API key is needed in your local.py')
    headers = {'Content-Type': 'application/xml', 'x-api-key': key}
    request = urllib2.Request(env.newrelic_deploy_url, data, headers)
    page = urllib2.urlopen(request, timeout=3)


@task
def update_newrelic_conf():
    """ update newrelic app monitoring configuration """
    require('hosts')
    if 'newrelic' in env.worker and env.newrelic_license_key is not None \
        and isfile(env.newrelic_template):
        print('%s Updating newrelic config' % MSG_IDENT),
        show_status(
            upload_template(env.newrelic_template, env.newrelic_cfg, context=env, backup=False, use_sudo=False)
        )


def set_perms():
    print('%s Setting permissions' % MSG_IDENT),
    show_status(run("chown %(unix_user)s:%(unix_group)s -hR %(root_dir)s" % env))


def _unchain_the_django():
    execute(collectstatic)
    execute(db_migration)


def _refresh_services():
    execute(check_log)
    execute(update_newrelic_conf)
    execute(push_supervisor_conf)
    execute(update_nginx_conf)
    execute(workers_reload)
    execute(nginx_reload)


@task
@with_settings(warn_only=True)
def update():
    """ update project $ fab <node> update"""
    local('git push')
    for current_host in env.hosts:
        with cd("%s/%s" % (env.directory, env.project_dir)):
            execute(get_pull)
            _unchain_the_django()
            execute(set_perms)
            _refresh_services()
            # _send_deploy_info()


@task
@with_settings(warn_only=True)
def push_local_settings():
    """ push `local.py` settings to node """
    print('%s Coping sensitive local settings to node' % MSG_IDENT),
    with cd("%(directory)s/%(project_dir)s/%(project)s" % env):
        show_status(put('%s/local.py' % env.project, 'local.py'))


@with_settings(warn_only=True)
def init_static():
    print('%s Creating static/media dirs...' % MSG_IDENT),
    _mkdir = lambda s, env=env: 'mkdir -p %(directory)s/%(project_dir)s' % env + '/%s' % s.split('/')[-1]
    show_status(
        run(_mkdir(settings.STATIC_ROOT)),
        run(_mkdir(settings.MEDIA_ROOT))
    )


@task
def collectstatic():
    """ Fires up a collectstatic task on the given node """
    with virtualenv():
        print('%s Collecting static files ...' % MSG_IDENT),
        show_status(run('python %s/manage.py collectstatic --noinput' % env.project_dir))


@task
@with_settings(warn_only=True)
def deploy(*args):
    """ deploy project $ fab <node> deploy"""
    require('hosts')
    for current_host in env.hosts:
        print('%s NODE: %s' % (MSG_IDENT, cyan(current_host)))
        if 'init' in args:
            execute(init)
        if not any(map(lambda x: x in args, ['nodump', 'init'])):
            execute(dump_db)
        execute(check_log)
        execute(make_venv)
        if 'init' in args:
            execute(install)
            execute(clone)
            execute(push_local_settings)
            execute(init_static)
            _unchain_the_django()
            execute(set_perms)
            _refresh_services()
        else:
            execute(update)
