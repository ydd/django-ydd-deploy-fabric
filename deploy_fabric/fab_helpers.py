# coding: utf-8
__author__ = 'voleg'

def FileToList(filename):
    filetolist = ''
    try:
        filetolist = open(filename, 'r').readlines()
    except IOError:
        print("File not found")
    return filetolist
