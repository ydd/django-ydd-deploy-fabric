# coding: utf-8
from distutils.core import setup
from setuptools import find_packages

setup(
    name='django-ydd-deploy-fabric',
    version="0.2.1",
    description="Generic deploy fabric used in YDD team",
    long_description="",
    keywords='django, fabric, deploy',
    author='Oleg Sapunov',
    author_email='ffrooty@gmail.com',
    url='https://bitbucket.org/ydd/django-ydd-deploy-fabric',
    license='BSD',
    include_package_data=True,
    packages=find_packages(),
    install_requires=['django>=1.4', 'Fabric>=1.6']
)
